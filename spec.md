# t-activity-list Specification

### Component Use

``` html

<t-activity-list
  resources="{{resources}}"
  settings="{{settings}}"
  sorting="{{sorting}}"
  list="{{results}}"
>
</t-activity-list>

```


### Options to Component

```javascript

	var resources = {
	    "icons": {
	      "search": "search-icon",
	      "filter": "filter-icon",
	      "list": "list-icon",
	      "grid": "grid-icon",
	      "sort": "sort-icon",
	      "sort-by-asc": "arrow-down-icon",
	      "sort-by-desc": "arrow-up-icon"
	    },
	    "messages": {
	      "resultsFound": "We found [[resultCount]] Activities for your search.",
	      "noResultsFound": "Couldn't find any Activities with your search",
	      "noResultsFiltered": "Couldn't find any Activities with your filters",
	      "noResultsfilteredForSearchTerm": "Couldn't find any Activity names containing [[searchTerm]]."
	    },
	    "buttons": {
	      "options": "Show Options",
	      "book": "Book Now",
          "loadMore": "Show more activities",
	      "reset": "Reset Filter"
	    }
	  }

```

```javascript

	  var settings = {
	    "showPerPage": 20
	  }

```

#### Sorting Options

```javascript

	var sorting = [
	    {
	      "type": "price",
	      "label": "Price",
	      "defaultSortBy": "desc",
	      "selected": true,
	      "onClick": _sortBy('price')
	    },
	    {
	      "type": "name",
	      "label": "Name",
	      "defaultSortBy": "asc",
	      "selected": false,
	      "onClick": _sortBy('name')
	    },
	    {
	      "type": "category",
	      "label": "Category",
	      "defaultSortBy": "desc",
	      "selected": false,
	      "onClick": _sortBy('category')
	    }
	  ]
```


#### Results Data

```javascript

	var results = [
		    {
		      "title": "This is an activity name",
		      "description": "Beans are cool and they are beans.  Eat them.  They are yummy",
		      "category": {
		        "name": "Family Friendly"
		      },
		      "thumbnail": "http://www.dynamic.viator.com/graphicslib/6801/SITours/cool-beans-mod-in-las-vegas-168521.jpg",
		      "isCancellable": true,
		      "allPassengersInfoRequired": false,
		      "isGuaranteeRequired": true,
		      "name": "Cool Beans MOD",
		      "id": "",
		      "fare": {
		        "amount": 10.37,
		        "currency": "USD"
		      }
		    },
		    {
		      "title": "This is an activity name",
		      "description": "Beans are cool and they are beans.  Eat them.  They are yummy",
		      "category": {
		        "name": "Family Friendly"
		      },
		      "thumbnail": "http://www.dynamic.viator.com/graphicslib/6801/SITours/cool-beans-mod-in-las-vegas-168521.jpg",
		      "isCancellable": true,
		      "allPassengersInfoRequired": false,
		      "isGuaranteeRequired": true,
		      "name": "Cool Beans MOD",
		      "id": "",
		      "fare": {
		        "amount": 12.55,
		        "currency": "USD"
		      }
		    }
		  ]
```


## Important Information

- Misc items to be handled from CSS like:
1. List card background color
2. Mixin (to control text color, font etc)
3. Mixin (Button color, size etc.)


## Test Cases
- Basic validation for filter

## Steps to Start
- Set Github repository at your end for this project, we will merge them later
- APIs Integration - Use Tavisca's APIs for integration purpose.

## Performance standard
- Any component if opened via [web page tester](https://www.webpagetest.org/), it should load under 500ms (milli seconds).

## Documents
- Visual designs for search components - https://projects.invisionapp.com/share/6E9PJ7R4Q#/screens/212067485
- API access : Url - http://demo.travelnxt.com/dev
- Tavisca Elements - https://github.com/atomelements and https://github.com/travelnxtelements
- Vaadin elements - https://vaadin.com/docs/-/part/elements/elements-getting-started.html
- Google - https://elements.polymer-project.org/browse?package=google-web-components
- Tavisca Web component style Guide - https://drive.google.com/open?id=0B7BT_2nBFNYVR2tscE9pRnVJYmc

## Ballpark Estimates

5 Days / 25 Weeks
- Estimates are including above requrement with responsive design.
- Estimates are for single resourse.
- These are ballpark estimates and can be more/less while actual development.
